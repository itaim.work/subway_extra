using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisBlockController : MonoBehaviour
{

    public void SetActive()
    {
        gameObject.GetComponent<MeshRenderer>().enabled = true;
        gameObject.GetComponent<Rigidbody>().useGravity = true;
    }
}
