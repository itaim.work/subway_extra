using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject[] TrackPrefabs;

    private GameObject tempGameObject;

    [SerializeField] private Vector3 ExitPoint;
    int counter = 0;
    [SerializeField] private Transform Player;

    [SerializeField] private GameObject PauseMenu;
    [SerializeField] private GameObject LoseMenu;

    // Start is called before the first frame update
    void Start()
    {

        Player = GameObject.Find("Player").transform;

        TrackPrefabs = Resources.LoadAll<GameObject>("Prefabs");

        for (int i = 0; i < 20; i++)
        {
            tempGameObject = Instantiate(TrackPrefabs[Random.Range(0, TrackPrefabs.Length)], ExitPoint, Quaternion.identity);
            tempGameObject.transform.position = new Vector3(tempGameObject.transform.position.x - 15, tempGameObject.transform.position.y, tempGameObject.transform.position.z);
            ExitPoint = tempGameObject.transform.FindChild("ExitPoint").transform.position;

        }

    }

    // Update is called once per frame
    void Update()
    {
        if (tempGameObject.transform.position.x + 100 >= Player.position.x)
        {

            tempGameObject = Instantiate(TrackPrefabs[Random.Range(0, TrackPrefabs.Length)], ExitPoint, Quaternion.identity);
            tempGameObject.transform.position = new Vector3(tempGameObject.transform.position.x - 15, tempGameObject.transform.position.y, tempGameObject.transform.position.z);
            ExitPoint = tempGameObject.transform.FindChild("ExitPoint").transform.position;
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            Time.timeScale = 0;
            PauseMenu.SetActive(true);
        }
    }

    public void ResumeGame()
    {
        PauseMenu.SetActive(false);

        Time.timeScale = 1f;

    }

    public void RestartGame()
    {
        Time.timeScale = 1f;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void MainMenu()
    {
        Time.timeScale = 1f;

        SceneManager.LoadScene(0);

    }

    public void Lose()
    {
        Time.timeScale = 0;
        LoseMenu.SetActive(true);
    }
}
