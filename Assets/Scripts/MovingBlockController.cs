using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingBlockController : MonoBehaviour
{

    private bool Direction;

    [SerializeField] private float speed = 5f;

    [SerializeField] private Vector3 RightPos;
    [SerializeField] private Vector3 LeftPos;

    private void Start()
    {
        RightPos = new Vector3(transform.position.x, transform.position.y, 3);

        LeftPos = new Vector3(transform.position.x, transform.position.y, -3);

    }



    // Update is called once per frame
    void Update()
    {
        if (transform.position.z >= 3)
        {
            Direction = false;
        }

        if (transform.position.z <= -3)
        {
            Direction = true;
        }

        if (Direction)
        {

            transform.position = Vector3.MoveTowards(transform.position, RightPos, speed * Time.deltaTime);
        }

        else
        {
            transform.position = Vector3.MoveTowards(transform.position, LeftPos, speed * Time.deltaTime);

        }


    }
}
