using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerZoneController : MonoBehaviour
{
    [SerializeField] GameObject fatherObject;

    private void Start()
    {
        fatherObject = transform.parent.gameObject;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Equals("Player"))
        {
            fatherObject.GetComponent<InvisBlockController>().SetActive();
        }
    }

}
